using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBGManager : MonoBehaviour
{
    [SerializeField] private AudioClip menuBackground;
    [SerializeField] private AudioClip levelBackground;
    [SerializeField] private AudioClip GameOverBackground;

    [SerializeField] private SOGeneralOptions SOPersistent;

    private AudioSource audioSource;

    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void setVolume()
    {
        audioSource.volume = SOPersistent.audioBGVolume;
    }

    public void menuBackgroundAudioPlay()
    {
        audioSource.clip = menuBackground;
        audioSource.Play();
    }

    public void levelBackgroundAudioPlay()
    {
        audioSource.clip = levelBackground;
        audioSource.Play();
    }

    public void gameOverBackgroundAudioPlay()
    {
        audioSource.clip = GameOverBackground;
        audioSource.Play();
    }
}
