using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class EnemyWhitPatrol : Enemy
{

    [SerializeField] private GameObject poolShoot;

    private int destPoint = 0;
    private bool waitingForNextPath;

    private enum SwitchMachineStates { NONE, IDLEV, IDLEH, WALKV, WALKH, ATTACK, RECIVEHIT, DIE};
    private SwitchMachineStates currentState;
    private SwitchMachineStates lastState;
    private bool flip_h = false;
    private bool flip_v = false;

    private void Awake()
    {
        patrol = true;
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        statsEnemy = LevelManager.Instance.GetRNGPatrolEnemy();
        animator.runtimeAnimatorController = statsEnemy.animator;
        hpMax = statsEnemy.maxHP;
        hpActual = hpMax;
        moveSpeed = statsEnemy.moveSpeed;
        damage = statsEnemy.damage;
        rangeDetection = statsEnemy.rangeDetection;
        rangeAttack = statsEnemy.rangeAttack;
        GetComponent<SpriteRenderer>().color = statsEnemy.color;

        poolShoot = LevelManager.Instance.GetPoolShoot();

        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        agent.updateRotation = false;
        agent.updateUpAxis = false;

        waitingForNextPath = false;

        player = GameManager.Instance.GetPlayerController();

        agent.SetDestination(this.transform.position);
        InitState(SwitchMachineStates.IDLEV);
    }

    void Update()
    {

        UpdateState();

        playerRaycast = Physics2D.OverlapCircle(transform.position, rangeDetection, layerMask);

        if (!inmune && playerRaycast != null)
            TrackedPlayer();


        float distance = Vector2.Distance(agent.destination, this.transform.position);
        if (playerRaycast == null && playerTraked)
            StartCoroutine(StayOnSite());
        else if (distance < 0.4f && !waitingForNextPath && playerRaycast == null)
            StartCoroutine(CooldownToNextPoint());
    }

    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        yield return new WaitForSeconds(1);
        agent.speed = moveSpeed;
        playerTraked = false;
    }

    IEnumerator CooldownToNextPoint()
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(3f);
        GotoNextPoint();
    }

    void TrackedPlayer()
    {
        StopCoroutine(StayOnSite());
        agent.SetDestination(player.transform.position);
        float distance = Vector2.Distance(agent.destination, this.transform.position);
        if (distance < rangeAttack)
        {
            agent.speed = 0;
            if (canAttack)
                ChangeState(SwitchMachineStates.ATTACK);
        }
        else
            agent.speed = moveSpeed;

    }

    private void ShootToPlayer()
    {
        canAttack = false;
        for (int i = 0; i < poolShoot.transform.childCount; i++)
        {
            Transform tActual = poolShoot.transform.GetChild(i);
            if (!tActual.gameObject.activeSelf)
            {
                tActual.gameObject.SetActive(true);
                tActual.transform.position = transform.position;
                tActual.gameObject.GetComponent<Rigidbody2D>().velocity = (agent.destination - tActual.transform.position).normalized * 8;
                tActual.gameObject.GetComponent<BulletControl>().SetBulletDamage(damage);
                break;
            }
        }
        StartCoroutine(ShootCoolDown(3));
    }

    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canAttack = true;
    }

    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Count == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.SetDestination(points[destPoint].transform.position);
        agent.speed = moveSpeed;
        waitingForNextPath = false;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Count;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (!inmune || collision.gameObject.GetComponentInParent<PlayerController>().comboDone)
        {
            collision.gameObject.GetComponentInParent<PlayerController>().comboDone = false;
            inmune = true;
            hpActual -= collision.gameObject.GetComponent<HitboxInfo>().hitDamage;
            if (hpActual <= 0)
                ChangeState(SwitchMachineStates.DIE);
            else
                ChangeState(SwitchMachineStates.RECIVEHIT);
        }
        
    }


    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLEV:

                agent.speed = 0;
                if (!flip_v)
                    animator.Play("IdleDown");
                else
                    animator.Play("IdleTop");
                break;

            case SwitchMachineStates.IDLEH:

                agent.speed = 0;
                animator.Play("IdleSide");
                break;

            case SwitchMachineStates.WALKV:

                if (flip_v)
                    animator.Play("WalkTop");
                else
                    animator.Play("WalkDown");

                break;

            case SwitchMachineStates.WALKH:
                if(flip_h && transform.rotation.y == 0)
                    transform.Rotate(0, 180, 0);
                else
                    transform.Rotate(0, -180, 0);

                animator.Play("WalkSide");

                break;

            case SwitchMachineStates.ATTACK:
                agent.speed = 0;
                switch (lastState)
                {
                    case SwitchMachineStates.IDLEH:
                    case SwitchMachineStates.WALKH:
                        animator.Play("AttackSide");

                        break;

                    case SwitchMachineStates.IDLEV:
                    case SwitchMachineStates.WALKV:
                        if (flip_v)
                            animator.Play("AttackTop");
                        else
                            animator.Play("AttackDown");

                        break;

                }
                break;

            case SwitchMachineStates.RECIVEHIT:
                agent.speed = 0;
                GetComponent<SpriteRenderer>().color = Color.red;
                StartCoroutine(StunTime());
                break;

            case SwitchMachineStates.DIE:
                LevelManager.Instance.OnEnemyDie();
                Destroy(gameObject);
                break;

            default:
                break;
        }
    }
    IEnumerator StunTime()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(lastState);
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLEV:

                break;

            case SwitchMachineStates.WALKV:

                break;

            case SwitchMachineStates.IDLEH:

                break;

            case SwitchMachineStates.WALKH:

                break;

            case SwitchMachineStates.ATTACK:
                ShootToPlayer();
                break;

            case SwitchMachineStates.RECIVEHIT:
                inmune = false;
                GetComponent<SpriteRenderer>().color = statsEnemy.color;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        Vector3 velocityNormalized = agent.velocity.normalized;
        switch (currentState)
        {
            case SwitchMachineStates.IDLEV:
            case SwitchMachineStates.IDLEH:

                if (agent.speed != 0)
                {
                    if (Mathf.Abs(velocityNormalized.y) > Mathf.Abs(velocityNormalized.x))
                    {
                        if (agent.velocity.y < 0)
                            flip_v = false;
                        else if (agent.velocity.y > 0)
                            flip_v = true;
                        ChangeState(SwitchMachineStates.WALKV);
                    }
                    if (Mathf.Abs(velocityNormalized.y) < Mathf.Abs(velocityNormalized.x))
                    {
                        if (velocityNormalized.x < 0)
                            flip_h = false;
                        if (velocityNormalized.x > 0)
                            flip_h = true;
                        ChangeState(SwitchMachineStates.WALKH);

                    }
                }
                
                lastState = currentState;
                break;

            case SwitchMachineStates.WALKV:
                
                if (agent.speed == 0)
                    ChangeState(SwitchMachineStates.IDLEV);
                if (Mathf.Abs(velocityNormalized.y) < Mathf.Abs(velocityNormalized.x))
                {
                    if (velocityNormalized.x < 0)
                        flip_h = false;
                    if(velocityNormalized.x > 0)
                        flip_h = true;
                    ChangeState(SwitchMachineStates.WALKH);
                }

                lastState = currentState;
                break;


            case SwitchMachineStates.WALKH:

                if (agent.speed == 0)
                    ChangeState(SwitchMachineStates.IDLEH); 
                if (Mathf.Abs(velocityNormalized.x) < Mathf.Abs(velocityNormalized.y))
                {
                    if (agent.velocity.y < 0)
                        flip_v = false;
                    else if (agent.velocity.y > 0)
                        flip_v = true;
                    ChangeState(SwitchMachineStates.WALKV);
                }

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.RECIVEHIT: 
                break;
            case SwitchMachineStates.DIE: 
                break;

            default:
                break;
        }
    }

    private void EndAnimation()
    {
        ChangeState(lastState);
    }
    #endregion
}
