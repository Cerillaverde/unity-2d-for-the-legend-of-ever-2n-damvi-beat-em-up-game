using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected LayerMask layerMask;
    protected List<Transform> points;
    protected SOEnemy statsEnemy;

    protected bool patrol;
    protected Animator animator;
    protected int hpMax;
    protected int hpActual;
    protected float moveSpeed;
    protected int damage;
    protected float rangeAttack;
    protected float rangeDetection;
    protected Color color;

    protected bool playerTraked;

    protected PlayerController player;
    protected NavMeshAgent agent;

    protected bool canAttack = true;
    protected bool inmune = false;
    protected Collider2D playerRaycast;

    public bool IsEnemyWhitPatrol()
    {
        return patrol;
    }

    public void SetPointsOfPatrol(List<Transform> points)
    {
        this.points = points;
    }
    
}
