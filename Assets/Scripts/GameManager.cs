using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    [HideInInspector] public int round;

    public AudioBGManager audioBGManager;
    public AudioSFXManager audioSFXManager;
    [SerializeField] private SOGeneralOptions SOPersistent;

    [Space(10)]
    [Header("Level Canvas")]
    [SerializeField] private GameObject levelCanvas;
    [SerializeField] private TMP_Text enemiesInCanvas;
    [SerializeField] private TMP_Text roundInCanvas;
    [SerializeField] private TMP_Text roundTimeInCanvas;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private TMP_Text countDownCanvas;
    [SerializeField] private GameObject infoGameCanvas;
    [SerializeField] private Slider healthBar;

    [Space(10)]
    [Header("Menu Canvas")]
    [SerializeField] private GameObject menuCanvas;

    [Space(10)]
    [Header("Menu GameOver")]
    [SerializeField] private GameObject GameOverCanvas;
    [SerializeField] private TMP_Text infoRoundEnd;

    [Space(10)]
    [Header("Menu Options")]
    [SerializeField] private GameObject optionsCanvas;

    private bool gameIsPaused = false;
    public bool GetGameIsPaused() { return gameIsPaused; }

    private PlayerController playerController;
    public PlayerController GetPlayerController() { return playerController; }


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        levelCanvas.SetActive(false);
        GameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);

        audioBGManager.setVolume();
        audioSFXManager.setVolume();

        LoadMenuScene();
    }

    #region Player Control

    internal void SetPlayer(PlayerController playerController)
    {
        this.playerController = playerController;
    }

    public void DestoyPlayer()
    {
        Destroy(playerController);
    }

    #endregion

    #region Manage Scene
    public void LoadLevelScene()
    {
        SceneManager.LoadScene("LevelScene");
        IntoLevelScene();
    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene("MenuScene");
        IntoMenuScene();
    }

    public void LoadGameOverScene()
    {
        SceneManager.LoadScene("GameOver");
        IntoGameOverScene();
    }

    public void IntoLevelScene()
    {

        menuCanvas.SetActive(false);
        levelCanvas.SetActive(true);

        audioBGManager.levelBackgroundAudioPlay();
    }

    public void IntoMenuScene()
    {
        GameOverCanvas.SetActive(false);
        levelCanvas.SetActive(false);

        menuCanvas.SetActive(true);
        audioBGManager.menuBackgroundAudioPlay();
    }

    internal void IntoGameOverScene()
    {
        levelCanvas.SetActive(false);
        GameOverCanvas.SetActive(true);
        infoRoundEnd.text = $"Llegaste a la ronda {round}";
        audioBGManager.gameOverBackgroundAudioPlay();
    }

    #endregion

    #region Level Control

    public void SetMaxHealtBar(int health)
    {
        healthBar.maxValue = health;
        healthBar.value = health;
    }

    public void SetHealtBar(int health)
    {
        healthBar.value = health;
    }

    public void SetEnemiesCanvas(int enemiesAlive)
    {
        enemiesInCanvas.text = "Enemies: " + enemiesAlive;
    }

    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }
    public void ReturnToMenu()
    {
        ResumeGame();
        LoadMenuScene();
    }

    public void ActiveInfoFirstRound()
    {
        infoGameCanvas.SetActive(true);
    }

    public void DesactiveInfoFirstRound()
    {
        infoGameCanvas.SetActive(false);
    }

    public void ActualRoundInCanvas()
    {
        roundInCanvas.text = $"Round {round}";
    }

    public void CountDownNextLevel(int t)
    {
        countDownCanvas.gameObject.SetActive(true);
        if (t > 0)
        {
            countDownCanvas.text = $"Next Level\nin\n{t}";
        }
        else
        {
            countDownCanvas.text = "Level START!";
        }
    }

    public void CountDownRoundTimeLeft(int t)
    {
        if (t > 0)
        {
            roundTimeInCanvas.text = $"Time Left: {t}";
        }
    }

    internal void DisableCountDown()
    {
        countDownCanvas.gameObject.SetActive(false);
    }

    #endregion

    #region Option Control

    public void ActiveCanvasOptions()
    {
        pauseMenu.SetActive(false);
        optionsCanvas.SetActive(true);
    }

    public void DisableCanvasOptions()
    {
        if (gameIsPaused)
            pauseMenu.SetActive(true);

        optionsCanvas.SetActive(false);
    }

    #endregion

}
