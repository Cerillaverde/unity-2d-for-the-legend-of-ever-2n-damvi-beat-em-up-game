using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance = null;

    [SerializeField] private GameObject poolShoot;
    [SerializeField] private SOEnemy[] statsNormalEnemy;
    [SerializeField] private SOEnemy[] statsPatrolEnemy;
    [SerializeField] private SpawnControl spawner;
    [SerializeField] private SOGeneralOptions SOPersistent;
    [SerializeField] private GameObject blockedLevel;

    [HideInInspector] public int enemiesAlive;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GameManager.Instance.round = 1;
        NextLevel();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.Instance.GetGameIsPaused())
            {
                GameManager.Instance.ResumeGame();
            }
            else
            {
                GameManager.Instance.PauseGame();
            }
        }
    }

    #region Enemies Control

    public GameObject GetPoolShoot()
    {
        return poolShoot;
    }

    public SOEnemy GetRNGNormalEnemy()
    {
        return statsNormalEnemy[UnityEngine.Random.Range(0, statsNormalEnemy.Length)];
    }

    public SOEnemy GetRNGPatrolEnemy()
    {
        return statsPatrolEnemy[UnityEngine.Random.Range(0, statsPatrolEnemy.Length)];
    }

    public void OnEnemyDie()
    {
        enemiesAlive--;
        GameManager.Instance.SetEnemiesCanvas(enemiesAlive);

        if (enemiesAlive <= 0)
            EndLevel();

    }

    #endregion

    #region Level Control

    public void StartLevel()
    {
        blockedLevel.SetActive(false);
        GameManager.Instance.ActualRoundInCanvas();
        spawner.SpawnEnemies();
        GameManager.Instance.SetEnemiesCanvas(enemiesAlive);
        StartCoroutine(RoundTimeLeft(SOPersistent.timeOfRound + (10 * GameManager.Instance.round)));
    }

    IEnumerator RoundTimeLeft(int time)
    {
        if (time < 0)
        {
            GameManager.Instance.LoadGameOverScene();
        }
        else
        {
            GameManager.Instance.CountDownRoundTimeLeft(time);
            yield return new WaitForSeconds(1);
            StartCoroutine(RoundTimeLeft(time - 1));
        }
    }

    public void NextLevel()
    {
        blockedLevel.SetActive(true);
        StartCoroutine(LvlStart(5));
    }

    public void EndLevel()
    {
        StopAllCoroutines();
        GameManager.Instance.GetPlayerController().ResetHP();
        GameManager.Instance.round++;
        GameManager.Instance.GetPlayerController().gameObject.transform.position = new Vector2(0, 0);
        NextLevel();
    }
    public void StopEndLevel()
    {
        StopAllCoroutines();
        NextLevel();
    }
    IEnumerator LvlStart(int t)
    {
        if (t < 0)
        {
            StartLevel();
            GameManager.Instance.DisableCountDown();
        }
        else
        {
            GameManager.Instance.CountDownNextLevel(t);
            yield return new WaitForSeconds(1);
            StartCoroutine(LvlStart(t - 1));
        }

    }

    #endregion
}
