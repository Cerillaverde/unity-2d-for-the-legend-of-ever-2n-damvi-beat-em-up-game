using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControl : MonoBehaviour
{
    private int hitDamage;
    public void SetBulletDamage(int damage) { hitDamage = damage; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<HitboxInfo>().hitDamage = hitDamage;
        gameObject.SetActive(false);
    }


}
