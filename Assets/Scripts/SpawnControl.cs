using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class SpawnControl : MonoBehaviour
{

    [SerializeField] GameObject[] spawnPoints;
    [SerializeField] GameObject[] enemyList;

    public void SpawnEnemies()
    {

        int enemyNumber = 3 * GameManager.Instance.round;

        LevelManager.Instance.enemiesAlive = enemyNumber;
        GameManager.Instance.SetEnemiesCanvas(enemyNumber);
        for (int i = 0; i < enemyNumber; i++)
        {
            GameObject newMob;

            if (GameManager.Instance.round == 1)
                newMob = Instantiate(enemyList[1]);
            else if (GameManager.Instance.round == 2)
                newMob = Instantiate(enemyList[0]);
            else
                newMob = Instantiate(enemyList[Random.Range(0, enemyList.Length)]);

            NavMeshAgent mobNav = newMob.GetComponent<NavMeshAgent>();

            int spawnPoint = Random.Range(0, spawnPoints.Length);
            mobNav.Warp(spawnPoints[spawnPoint].transform.position);
            if (newMob.GetComponent<Enemy>().IsEnemyWhitPatrol())
            {
                List<Transform> list = new List<Transform>();
                for (int j = 0; j < spawnPoints[spawnPoint].transform.childCount; j++)
                {
                    list.Add(spawnPoints[spawnPoint].transform.GetChild(j));
                }
                newMob.GetComponent<EnemyWhitPatrol>().SetPointsOfPatrol(list);
            }
        }

    }

}
