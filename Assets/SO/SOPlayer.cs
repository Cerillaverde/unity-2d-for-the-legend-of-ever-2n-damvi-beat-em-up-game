using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SOPlayer", menuName = "Scriptable Objects/SO Player")]
public class SOPlayer : ScriptableObject
{

    public float speed;
    public int hpMax;
    public int attack1Damage;
    public int attack2Damage;
    public int comboDamage;

}
