using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu(fileName = "SOEnemy", menuName = "Scriptable Objects/SO Enemy")]
public class SOEnemy : ScriptableObject
{
    [Header("Atributtes of enemie")]
    public AnimatorController animator;

    [Space(2)]
    public Color color;
    [Space(2)]
    public int maxHP;
    [Space(2)]
    public float moveSpeed;
    [Space(2)]
    public int damage;

    [Range(5, 20)]
    [Tooltip("Determines the enemy's detection range towards the player.")]
    [Space(2)]
    public float rangeDetection;

    [Range(0.5f, 10)]
    [Tooltip("Determines the enemy's attack range.")]
    [Space(2)]
    public float rangeAttack;

    

}
